#ifndef REQUEST_H
#define REQUEST_H

#include "config.h"

typedef void (*complete_callback)(request_t *);
typedef int  (*error_callback)(request_t *, int);

struct request {
	complete_callback on_complete;
	error_callback    on_error;
};

INLINE void request_set_callbacks(request_t *req, complete_callback on_complete, error_callback on_error) {
	req->on_complete = on_complete;
	req->on_error = on_error;
}

#endif
