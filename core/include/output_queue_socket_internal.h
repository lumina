#ifndef OUTPUT_QUEUE_INTERNAL_H
#define OUTPUT_QUEUE_INTERNAL_H
#include <ev.h>

#include "output_queue.h"

typedef struct Target_Socket Target_Socket;
struct Target_Socket {
	Target base;
	ev_io writable;
	DataItem_queue queue;
	int current_position;
};

#endif
