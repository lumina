#include "listen_handler.h"
#include <stdlib.h> /* calloc */

listen_handler_t *new_listen_handler(core_t *c) {
	listen_handler_t *h = calloc(1, sizeof(listen_handler_t));
	if(!h) goto fail;
	h->core = c;
	return h;
fail:
	free_listen_handler(h);
	return NULL;
}

void free_listen_handler(listen_handler_t *h) {
	if(!h) return;
	free(h);
}
