#ifndef OUT_STRATEGY_H
#define OUT_STRATEGY_H

#include "data_item.h"
#include "output_queue.h"

typedef enum OutStrategy_Result {
	OS_RESULT_SKIP,   /* Skip this output strategy */
	OS_RESULT_COMPLETE, /* Output is complete */
	OS_RESULT_WAIT      /* Output needs waiting */
} OutStrategy_Result;

typedef struct OutStrategy OutStrategy;

struct OutStrategy {
	/* List of types to assist potentially optimizing dispatch implementations */
	DataItemType *handledData;
	TargetType *handledTargets;
	OutStrategy_Result (*put)(OutStrategy *, DataItem *, Target *);
	void (*free)(OutStrategy*);
};

INLINE OutStrategy_Result OutStrategy_put(OutStrategy *strategy, DataItem *item, Target *target) {
	assert(strategy && strategy->put);
	return strategy->put(strategy, item, target);
}
INLINE void OutStrategy_free(OutStrategy *strategy) {
	assert(strategy && strategy->free);
	strategy->free(strategy);
}
#endif
