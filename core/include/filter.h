#ifndef FILTER_H
#define FILTER_H

#include "config.h"
#include "data_item.h"

struct filter;
typedef struct filter filter_t;

typedef enum {
	FILTER_WRITE,
	FILTER_WRITE_COMPLETE,
	FILTER_READ,
	FILTER_READ_COMPLETE
} FilterMode;

typedef void (*FilterAction_next)(void *, FilterMode mode, DataItem *);
typedef void (*FilterAction)(void *, FilterMode mode, DataItem *, FilterAction_next, void *);

typedef struct SimpleFilter {
	FilterAction onWrite;
	FilterAction onWriteComplete;
	FilterAction onRead;
	FilterAction onReadComplete;
} SimpleFilter;

void SimpleFilter_action(void *filter, FilterMode mode, DataItem *item, FilterAction_next next, void *nextData);

#endif
