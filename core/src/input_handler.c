#include "input_handler.h"
#include <stdlib.h> /* calloc */

input_handler_t *new_input_handler(core_t *c) {
	input_handler_t *h = calloc(1, sizeof(input_handler_t));
	if(!h) goto fail;
	h->core = c;
	return h;
fail:
	free_input_handler(h);
	return NULL;
}

void free_input_handler(input_handler_t *h) {
	if(!h) return;
	free(h);
}
