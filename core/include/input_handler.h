#ifndef INPUT_HANDLER_H
#define INPUT_HANDLER_H

#include "config.h"

struct input_handler {
	core_t *core;
};

input_handler_t *new_input_handler(core_t *c);
void free_input_handler(input_handler_t *h);

void input_handler_queue(input_handler_t *h, input_request_t *dr);

#endif
