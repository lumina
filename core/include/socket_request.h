#ifndef SOCKET_REQUEST_H
#define SOCKET_REQUEST_H

#include "config.h"

/* XXX: Needs serious evolution */
typedef struct HandlerCallback {
	void (*onComplete)(void*, void *);
	int  (*onError)(void*, int, void *);
	void *data;
} HandlerCallback;

SocketRequest *new_SocketRequest(core_t *core, HandlerCallback callback, byte domain, byte type, byte protocol);
void free_SocketRequest(SocketRequest *this);

void SocketRequest_queue(SocketRequest *sc, double retryInterval, int maxTries);

int SocketRequest_getFd(SocketRequest *sc);

core_t *SocketRequest_getCore(SocketRequest *sc);

#endif
