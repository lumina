#include <errno.h>
#include <stdio.h>
#include <memory.h>
#include <fcntl.h>
#include <ev.h>

#include <stdlib.h> /* exit, system */
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

/* setrlimit */
#include <sys/time.h>
#include <sys/resource.h>

#include <assert.h> /* assert */
#include "core.h"
#include "connect_request.h"
#include "request.h"

#include "data_item.h"
#include "data_item/memory_data_item.h"

#include "filters/FilterChain.h"

#if 0
void pump_data(output_item *item) {
	while(output_remaining(item)) {
		int ret = output_send(item, 1, FDT_PIPE);
		if(ret < 0) {
			perror("Failed to send output...");
			goto err;
		}
	}
	output_on_complete(item);
	goto cleanup;
err:
	output_on_error(item, errno);
cleanup:
	output_release(item);
}

void memory_test() {
	output_item *mem = new_memory_output_item("TEST VALUE\n", strlen("TEST VALUE\n"), 0);
	pump_data(mem);
}

output_item *new_file_output_item_byname(const char *name) {
	int fd = open(name, O_RDONLY);
	if(fd <= 0) {
		perror("Failed to open file");
		return NULL;
	}
	return new_file_output_item(fd);
}

void file_test() {
	output_item *file = new_file_output_item_byname("premake.lua");
	pump_data(file);
}

void event_test() {
	struct ev_loop *loop = ev_default_loop(0);
	output_hook *hook = new_output_hook();
	ev_io watcher;
	/* SET STDOUT to be non-blocking */
	int flags;

	output_item *mem = new_memory_output_item("TEST VALUE\n", strlen("TEST VALUE\n"), 0);
	output_hook_queue(hook, mem);
	mem = new_file_output_item_byname("core/src/output_hook.c");
	output_hook_queue(hook, mem);

	if(-1 == (flags = fcntl(1, F_GETFL, 0)))
		flags = 0;
	fcntl(1, F_SETFL, flags | O_NONBLOCK);
	ev_io_init(&watcher, output_hook_callback, /* STDOUT */ 1, EV_WRITE);
	watcher.data = hook;
	ev_io_start(loop, &watcher);

	ev_loop(loop, 0);
	release_output_hook(hook);
	ev_default_destroy();
	return;
}
#endif
static int bad_count = 0;
int connect_err_cb(void *req, int err, void *data) {
	int error = errno;
	int fd;
	bad_count++;
	printf("ERROR: %i %i %s\n", err, error, strerror(error));
	fd = ConnectRequest_getFd(req);
	if(fd > 0)
		close(fd);
	free_ConnectRequest(req);
	return 0;
}

static int success_count = 0;

void connect_cb(void *req, void *data) {
	int fd = ConnectRequest_getFd(req);
	success_count++;
	close(fd);
	free_ConnectRequest(req);
}

int socket_error_cb(void *req, int err, void *data) {
	int error = errno;
	int fd;
	bad_count++;
	printf("ERROR: %i %i %s\n", err, error, strerror(error));
	fd = SocketRequest_getFd(req);
	if(fd > 0 && error != EMFILE && error != ENFILE)
		close(fd);
	free(data);
	free_SocketRequest(req);
	return 0;
}

struct addrContainer {
	struct sockaddr *addr;
	socklen_t addr_len;
};

void socket_cb(void *req, void *data) {
	HandlerCallback callback = {
		connect_cb,
		connect_err_cb,
		NULL
	};
	core_t *c = SocketRequest_getCore(req);
	int fd = SocketRequest_getFd(req);
	struct addrContainer *addr = data;
	ConnectRequest *cr = new_ConnectRequest(c, callback, fd, addr->addr, addr->addr_len);
	ConnectRequest_queue(cr, 10);
	free(data);
	free_SocketRequest(req);
}

void connect_handler_test() {
	core_t *c = new_core();
	struct addrinfo ai, *aitop = NULL;
	int i;
	HandlerCallback callback = {
		socket_cb,
		socket_error_cb
	};
	assert(c);
	memset(&ai, 0, sizeof(ai));
	ai.ai_family = AF_INET;
	ai.ai_socktype = SOCK_STREAM;
	if(0 != getaddrinfo("localhost", "80", &ai, &aitop)) {
		perror("getaddrinfo"); exit(-1);
	}
	{
		struct rlimit lim;
		getrlimit(RLIMIT_NOFILE, &lim);
		lim.rlim_cur = 64;
		setrlimit(RLIMIT_NOFILE, &lim);
	}

	for(i = 0; i < 1024; i++) {
		SocketRequest *sr;
		struct addrContainer *cont = calloc(1, sizeof(struct addrContainer));
		cont->addr = aitop->ai_addr;
		cont->addr_len = aitop->ai_addrlen;
		callback.data = cont;
		sr = new_SocketRequest(c, callback, PF_INET, SOCK_STREAM, 0);
		SocketRequest_queue(sr, 0, 0);
	}
	freeaddrinfo(aitop);
	ev_loop(c->loop, 0);
	free_core(c);
	printf("BAD COUNT: %i\nGOOD COUNT: %i\nTOTAL:%i\n", bad_count, success_count, bad_count + success_count);
}
void DataItemTest() {
	DataItem *mem = new_MemoryItem(MEMORY_BUFFER("HELLO", 6));
	printf("MEMORY_TYPE: %i\n", DataItem_getTypeID(mem));
	free_DataItem(mem);
}

void test_FilterAction(void *state, FilterMode mode, DataItem *item, FilterAction_next next, void *nextData) {
	printf("GOT ACTION: %i - DATA: 0x%p\n", mode, item);
	next(nextData, mode, item);
}

void end_FilterAction(void *state, FilterMode mode, DataItem *item) {
	printf("DONE: %i - DATA: 0x%p\n", mode, item);
}
void chain_test() {
	void* chain = new_FilterChain();
	FilterChain_append(chain, test_FilterAction, NULL, NULL);
	FilterChain_action(chain, 0, NULL, end_FilterAction, NULL);
	free_FilterChain(chain);
}
int main(int argc, const char **argv) {
#if 0
	memory_test();
	file_test();
#endif
	DataItemTest();
	chain_test();
	if(argc > 1 && !strcmp(argv[1], "--sock")) {
		struct addrinfo ai, *aitop;
		int sock;
		system("nc -l -p 9876 &");
		sleep(1);
		memset(&ai, 0, sizeof(ai));
		ai.ai_family = AF_INET;
		ai.ai_socktype = SOCK_STREAM;
		if(0 != getaddrinfo("localhost", "9876", &ai, &aitop)) {
			perror("getaddrinfo"); return -1;
		}
		sock = socket(PF_INET, SOCK_STREAM, 0);
		if(sock < 0) { perror("Failed to create socket"); return -1; }
		if(0 > connect(sock, aitop->ai_addr, aitop->ai_addrlen)) {
			perror("connect");
			freeaddrinfo(aitop);
			return -1;
		}
		freeaddrinfo(aitop);
		/* Replace stdout w/ the socket */
		if(0 > dup2(sock, 1)) {
			perror("dup2"); return -1;
		}
	}
#if 0
	event_test();
#endif
	connect_handler_test();
	return 0;
}
