#ifndef OUTPUT_ITEM_H
#define OUTPUT_ITEM_H

#include <sys/types.h>
#include <sys/queue.h>

#include "fdtypes.h"

/* TODO: RELOCATE */
typedef unsigned char BYTE;

struct output_item;
typedef struct output_item output_item;

typedef struct {
	size_t (*get_size)(output_item *this);

	/* Handle windows handles? */
	/* TODO: Define return values */
	int (*send)(output_item* this, int fd, int fdtype);
	size_t (*remaining)(output_item *this);
	void (*release)(output_item *this);
	/* prepare and on_* -> Optional */
	void (*prepare)(output_item *this);
	void (*on_complete)(output_item *this);
	void (*on_error)(output_item *this, int status);
} output_item_ops;

struct output_item {
	output_item_ops *ops;
	/* User callbacks : Optional */
	void (*on_complete)(output_item *this);
	void (*on_error)(output_item *this, int status);
	SIMPLEQ_ENTRY(output_item) queue_entries;
};

#ifndef INLINE
#define INLINE static inline
#endif

INLINE size_t output_get_size(output_item *item);

INLINE void output_prepare(output_item *item);

INLINE int output_send(output_item *item, int fd, int fdtype);

INLINE size_t output_remaining(output_item *item);

INLINE void output_on_complete(output_item *item);

INLINE void output_on_error(output_item *item, int status);

INLINE void output_release(output_item *item);

#include "../src/output_item.inc"

#endif
