#ifndef MEMORY_DATA_ITEM_H
#define MEMORY_DATA_ITEM_H

#include <data_item.h>

typedef struct MemoryBuffer {
	unsigned char *data;
	size_t len;
} MemoryBuffer;

typedef struct MemoryItem {
	DataItem base;
	MemoryBuffer buffer;
} MemoryItem;

#define MEMORY_BUFFER(data,len) ((MemoryBuffer){(unsigned char*)data, len})
DataItem *new_MemoryItem(MemoryBuffer buffer);

#endif
