#ifndef CONNECT_REQUEST_H
#define CONNECT_REQUEST_H

#include "config.h"

#include <sys/types.h>
#include <sys/socket.h> /* sockaddr */

#include "socket_request.h" /* HandlerCallback */

ConnectRequest *new_ConnectRequest(core_t *c, HandlerCallback callback, int fd, const struct sockaddr *addr, socklen_t addr_len);
void free_ConnectRequest(ConnectRequest *cr);

void ConnectRequest_queue(ConnectRequest *cr, double timeout);

int ConnectRequest_getFd(ConnectRequest *cr);

core_t *ConnectRequest_getCore(ConnectRequest *cr);
#endif
