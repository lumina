#ifndef OUTPUT_HANDLER_H
#define OUTPUT_HANDLER_H

#include "config.h"

struct output_handler {
	core_t *core;
};

output_handler_t *new_output_handler(core_t *c);
void free_output_handler(output_handler_t *h);

void output_handler_queue(output_handler_t *h, output_request_t *dr);

#endif
