#ifndef CORE_H
#define CORE_H

#include "types.h"

struct core {
	struct ev_loop    *loop;
	dns_handler_t     *dns;
	listen_handler_t  *listen;
	input_handler_t   *input;
};

core_t *new_core();
void free_core(core_t *c);

#endif
