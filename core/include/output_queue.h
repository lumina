#ifndef OUTPUT_QUEUE_H
#define OUTPUT_QUEUE_H

#include "config.h"
#include "data_item.h"
#include "core.h"

#include <assert.h> /* assert */

typedef struct OutputQueue OutputQueue;
typedef struct OutputQueue_ops OutputQueue_ops;

typedef struct Target Target;

struct OutputQueue {
	OutputQueue_ops *ops;
};
struct OutputQueue_ops {
	void (*add)(OutputQueue *, DataItem *, Target *);
	void (*free)(OutputQueue *);
};

typedef enum TargetType {
	TARGET_SOCKET = 1
} TargetType;

struct Target {
	TargetType typeID;
};
INLINE void free_OutputQueue(OutputQueue *queue) {
	assert(queue && queue->ops && queue->ops->free);
	queue->ops->free(queue);
}

INLINE void OutputQueue_add_DataItem(OutputQueue *queue, DataItem *item, Target *target) {
	assert(queue && queue->ops && queue->ops->add);
	queue->ops->add(queue, item, target);
}

#endif
