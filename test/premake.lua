package.name = "lumina.test"
package.kind = "exe"
package.language = "c++"

package.links = {
	"lumina.core"
}

package.includepaths = {
	"include",
	"../core/include"
}
if linux then
	package.buildoptions = { "-Wall" }
	package.config["Debug"].buildoptions = { "-O0" }
else
	print([[Other environments currently untested, may need tweaking]])
end

package.files = {
	matchrecursive(
		"src/*.c",
		"include/*.h"
	)
}
