#include "filters/FilterChain.h"

#include <assert.h> /* assert */
#include <memory.h> /* memmove */

typedef struct FilterPair {
	FilterAction action;
	void *actionState;
	Filter_stateRelease stateRelease;
} FilterPair;

typedef struct FilterChain {
	FilterPair *actions;
	size_t numActions;
} FilterChain;

typedef struct FilterChain_state {
	FilterChain *chain;
	size_t index;
	FilterAction_next next;
	void *nextData;
} FilterChain_State;

static void FilterChain_next(void *nextData, FilterMode mode, DataItem *item);

static void FilterChain_call_current(FilterChain_State *state, FilterMode mode, DataItem *item) {
	int index = state->index;
	/* Handle completion cases */
	if(index < 0 || index >= state->chain->numActions) {
		state->next(state->nextData, mode, item);
		/* When complete, self-free the state */
		free(state);
		return;
	}
	state->chain->actions[index].action(state->chain->actions[index].actionState, mode, item, FilterChain_next, state);
}
static void FilterChain_next(void *nextData, FilterMode mode, DataItem *item) {
	FilterChain_State *state = (FilterChain_State*)nextData;
	switch(mode) {
	case FILTER_READ:
	case FILTER_WRITE_COMPLETE:
		state->index--;
		break;
	case FILTER_WRITE:
	case FILTER_READ_COMPLETE:
	default:
		state->index++;
		break;
	}
	FilterChain_call_current(state, mode, item);
}

void FilterChain_action(void *filter, FilterMode mode, DataItem *item, FilterAction_next next, void *nextData) {
	FilterChain *chain = (FilterChain*)filter;
	FilterChain_State *state;
	assert(filter && next);
	state = calloc(1, sizeof(FilterChain_State));
	if(!state) goto fail;
	state->chain = chain;
	/* Setup the start-point */
	switch(mode) {
	case FILTER_READ:
	case FILTER_WRITE_COMPLETE:
		state->index = chain->numActions - 1;
		break;
	case FILTER_WRITE:
	case FILTER_READ_COMPLETE:
	default:
		state->index = 0;
		break;
	}
	state->next = next;
	state->nextData = nextData;
	/* Begin the chain */
	FilterChain_call_current(state, mode, item);
	return;
fail:
	if(state) free(state);
}

void *new_FilterChain() {
	FilterChain *chain = (FilterChain*)calloc(1, sizeof(FilterChain));
	if(!chain) goto fail;
	return chain;
fail:
	if(chain) free(chain);
	return NULL;
}
void free_FilterChain(void *state) {
	FilterChain *chain = (FilterChain*)state;
	int idx;
	/* Free all chain items */
	for(idx = 0; idx < chain->numActions; idx++) {
		if(chain->actions[idx].stateRelease) {
			chain->actions[idx].stateRelease(chain->actions[idx].actionState);
		}
	}
	if(chain->actions) {
		free(chain->actions);
	}
	free(chain);
}

/* Naive in that it doesn't even try to act like a 'vector' */
static void resize_chain(FilterChain *chain, int newSize) {
	/* XXX: If out of memory, this will abort */
	FilterPair *newActions = realloc(chain->actions, newSize * sizeof(FilterPair));
	assert(newSize == 0 || newActions);
	chain->actions = newActions;
	chain->numActions = newSize;
}

static void set_chain_action(FilterChain *chain, int index, FilterAction action, void *actionState, Filter_stateRelease release) {
	chain->actions[index].action = action;
	chain->actions[index].actionState = actionState;
	chain->actions[index].stateRelease = release;
}

void FilterChain_append(void *state, FilterAction action, void *actionState, Filter_stateRelease release) {
	FilterChain *chain = (FilterChain*)state;
	int newPos = chain->numActions;
	resize_chain(chain, newPos + 1);
	set_chain_action(chain, newPos, action, actionState, release);
}

void FilterChain_prepend(void *state, FilterAction action, void *actionState, Filter_stateRelease release) {
	FilterChain *chain = (FilterChain*)state;
	int oldLen = chain->numActions;
	resize_chain(chain, chain->numActions + 1);
	memmove(chain->actions + sizeof(FilterPair), chain->actions, oldLen * sizeof(FilterPair));
	set_chain_action(chain, 0, action, actionState, release);
}

