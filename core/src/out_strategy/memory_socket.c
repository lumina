#include "out_strategy.h"
#include "out_strategy/memory_socket.h"
#include "output_queue_socket_internal.h"

#include "data_item/memory_data_item.h"

#include <errno.h> /* errno, EAGAIN */
#include <sys/types.h>
#include <sys/socket.h> /* send */

#include <stdio.h> /* printf - in temporary area */

static DataItemType handledData[] = { DATA_ITEM_MEMORY, 0 };
static TargetType handledTargets[] = { TARGET_SOCKET, 0 };

static OutStrategy_Result OutStrategy_Memory_Socket_put(OutStrategy *strategy, DataItem *item, Target *target) {
	MemoryItem *mem;
	Target_Socket *socket;
	int fd;
	assert(strategy && item && target);
	if(DataItem_getTypeID(item) != DATA_ITEM_MEMORY
		|| target->typeID != TARGET_SOCKET)
		return OS_RESULT_SKIP;
	mem = (MemoryItem*)item;
	fd = socket->writable.fd;
	while(1) {
		int ret = send(fd, mem->buffer.data + socket->current_position, mem->buffer.len - socket->current_position, 0);
		if(ret > 0) {
			socket->current_position += ret;
			if(socket->current_position == mem->buffer.len) { /* Done */
				socket->current_position = 0;
				return OS_RESULT_COMPLETE;
			}
		} else {
			if(errno == EAGAIN) /* Need to try later */
				return OS_RESULT_WAIT;
			/* ERROR! */
			printf("FAILED TO WRITE!\n");
			return OS_RESULT_COMPLETE; /* ??? What to do here ??? */
		}
	}
}

static void free_OutStrategy_Memory_Socket(OutStrategy *strategy) {
	free(strategy);
}

OutStrategy *new_OutStrategy_Memory_Socket() {
	OutStrategy *strategy = (OutStrategy*)calloc(1, sizeof(OutStrategy));
	if(!strategy) goto fail;
	strategy->handledData = handledData;
	strategy->handledTargets = handledTargets;
	strategy->put = OutStrategy_Memory_Socket_put;
	strategy->free = free_OutStrategy_Memory_Socket;
	return strategy;
fail:
	if(strategy) free(strategy);
	return NULL;
}
