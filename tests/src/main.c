#include <stdlib.h>
#include <check.h>

#include "test_list.h"

int main(int argc, const char** argv) {
	int number_failed;
	SRunner *sr;
	sr = srunner_create (NULL);
	srunner_add_suite(sr, new_filter_suite());
	srunner_run_all (sr, CK_NORMAL);
	number_failed = srunner_ntests_failed (sr);
	srunner_free (sr);
	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
