#include "output_queue.h"
#include "output_queue_socket.h"
#include "output_queue_socket_internal.h"

#include "out_strategy/memory_socket.h"

#include <errno.h> /* errno */

#include <ev.h>

#include <sys/types.h>
#include <sys/socket.h> /* send */

#include <stdio.h> /* printf */
/* TODO: Setup generic 'queue' so that input and output are handled in same queue */

static void SocketOutput_add(OutputQueue *queue, DataItem *item, Target *target);
static void free_OutputQueue_Socket(OutputQueue *queue);

/* DEFAULT STRATEGIES */
static OutStrategy *strategies[2] = { NULL /* mem sock */, NULL };

static OutputQueue_ops OutputQueue_Socket_ops = {
	SocketOutput_add,
	free_OutputQueue_Socket
};

typedef struct OutputQueue_Socket {
	OutputQueue base;
	core_t *core;
} OutputQueue_Socket;

OutputQueue *new_OutputQueue_Socket(core_t *core) {
	OutputQueue_Socket *queue = calloc(1, sizeof(OutputQueue_Socket));
	if(!queue) goto fail;
	/* XXX: Initialize the strategies if not already done */
	if(!strategies[0])
		strategies[0] = new_OutStrategy_Memory_Socket();
	queue->base.ops = &OutputQueue_Socket_ops;
	queue->core = core;
	return (OutputQueue*)queue;
fail:
	return NULL;
}

static int OutputQueue_Socket_writeItem(Target_Socket *target, int fd, DataItem *item) {
	/* Unorthodox... more should be moved into the strategy.. */
	OutStrategy **ptr;
	for(ptr = strategies; ptr && *ptr; ptr++) {
		OutStrategy_Result result = (*ptr)->put(*ptr, item, (Target*)target);
		switch(result) {
		case OS_RESULT_WAIT:
			return 0;
		case OS_RESULT_COMPLETE:
			return 1;
		case OS_RESULT_SKIP:
			continue;
		default:
			/* AGK: dunno what do do.. */
			continue;
		}
	}
	/* AGK: No strategies handle this */
	return 1;
}

static void OutputQueue_Socket_writeCallback(struct ev_loop *loop, struct ev_io *w, int revents) {
	/* HANDLE DATA */
	Target_Socket *target = (Target_Socket*)w->data;
	DataItem_queue *queue = &target->queue;
	DataItem *item;
	while((item = STAILQ_FIRST(queue))) {
		/* WRITE - 0 == not complete, needs to wait */
		if(0 == OutputQueue_Socket_writeItem(target, w->fd, item))
			break;
		/* Remove the item from the queue and release */
		STAILQ_REMOVE_HEAD(queue, entries);
		free_DataItem(item);
	}
}


Target *new_Target_Socket(OutputQueue *queue, int fd) {
	/* Currently no timeout */
	Target_Socket *target = calloc(1, sizeof(Target_Socket));
	if(!target) goto fail;
	target->base.typeID = TARGET_SOCKET;
	/* INIT THE QUEUE */
	STAILQ_INIT(&target->queue);
	ev_io_init(&target->writable, OutputQueue_Socket_writeCallback, fd, EV_WRITE);
	target->writable.data = target;
	/* don't start until data queued... */
	return (Target*)target;
fail:
	if(target) free_Target_Socket(queue, (Target*)target);
	return NULL;
}
void free_Target_Socket(OutputQueue *queue, Target *target) {
	assert(target->typeID == TARGET_SOCKET);
	Target_Socket *t = (Target_Socket*)target;
	DataItem *item;
	DataItem_queue *q = &t->queue;
	/* Make sure IO listener stopped */
	if(ev_is_active(&t->writable))
		ev_io_stop(((OutputQueue_Socket*)queue)->core->loop, &t->writable);
	/* Release all items in the queue */
	/* Cannot use foreach because it expects the item to exist for the next iteration */
	while((item = STAILQ_FIRST(q))) {
		STAILQ_REMOVE_HEAD(q, entries);
		free_DataItem(item);
	}
	free(t);
}

static void SocketOutput_add(OutputQueue *queue, DataItem *item, Target *target) {
	Target_Socket *t = (Target_Socket*)target;
	STAILQ_INSERT_HEAD(&t->queue, item, entries);
	if(!ev_is_active(&t->writable))
		ev_io_start(((OutputQueue_Socket*)queue)->core->loop, &t->writable);
}
static void free_OutputQueue_Socket(OutputQueue *queue) {
	free(queue);
}
