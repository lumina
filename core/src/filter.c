#include "filter.h"

#include <assert.h> /* assert */

/* Since the behavior for every 'simply filtered' item is the same, use a macro */
#define CASE_FILTER_ACTION(condition,function) \
	case condition: \
		if(f->function) { \
			f->function(filter, mode, item, next, nextData); \
		} else { \
			next(nextData, mode, item); \
		} \
		break;

void SimpleFilter_action(void *filter, FilterMode mode, DataItem *item, FilterAction_next next, void *nextData) {
	SimpleFilter *f = (SimpleFilter*)filter;
	assert(filter && next);

	switch(mode) {
	CASE_FILTER_ACTION(FILTER_WRITE, onWrite)
	CASE_FILTER_ACTION(FILTER_WRITE_COMPLETE, onWriteComplete)
	CASE_FILTER_ACTION(FILTER_READ, onRead)
	CASE_FILTER_ACTION(FILTER_READ_COMPLETE, onReadComplete)
	default:
		/* By default just pass it on no matter what it is*/
		next(nextData, mode, item);
		break;
	}
}
