#include "output_hook.h"
#include "output_item.h"
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <memory.h>
#include <malloc.h>

#include <sys/queue.h>

SIMPLEQ_HEAD(output_list, output_item);

struct output_hook {
	struct output_list queue;
};

output_hook *new_output_hook() {
	output_hook *ret = calloc(1, sizeof(output_hook));
	if(!ret) return NULL;
	SIMPLEQ_INIT(&ret->queue);
	return ret;
}

void release_output_hook(output_hook *hook) {
	if(hook) free(hook);
}

void output_hook_queue(output_hook *hook, output_item *item) {
	SIMPLEQ_INSERT_TAIL(&hook->queue, item, queue_entries);
}

static output_item *get_next(output_hook *hook) {
	output_item *next = SIMPLEQ_FIRST(&hook->queue);
	while(next && !output_remaining(next)) {
		SIMPLEQ_REMOVE_HEAD(&hook->queue, queue_entries);
		output_on_complete(next);
		/* PERFORM RELEASE... user callback should be doing this */
		output_release(next);
		next = SIMPLEQ_FIRST(&hook->queue);
	}
	return next;
}

void output_hook_callback(EV_P_ struct ev_io *w, int revents) {
	output_hook *hook = w->data;
	int fd = w->fd;
	ssize_t ret;
	output_item *next = get_next(hook);
	fprintf(stderr, "In output_hook\n");
	while(next) {
		/* ASSUME OUTPUT IS PIPE FOR NOW */
		ssize_t ret = output_send(next, fd, FDT_SOCKET);
		if(ret < 0) break; /* Error... output_item should have triggered */
		next = get_next(hook);
	}
	/* Completed normally */
	if(next == NULL) {
		ev_io_stop(EV_A_ w);
		return;
	}
	if(ret < 0 && errno != EAGAIN) {
		perror("Something went wrong while writing out to the fd");
		ev_io_stop(EV_A_ w);
	}
}
