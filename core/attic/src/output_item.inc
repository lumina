/* output_item inlined code */

#include <assert.h>

INLINE size_t output_get_size(output_item *item) {
	assert(item->ops && item->ops->get_size);
	return item->ops->get_size(item);
}

INLINE void output_prepare(output_item *item) {
	assert(item->ops);
	if(item->ops->prepare)
		item->ops->prepare(item);
}

INLINE int output_send(output_item *item, int fd, int fdtype) {
	assert(item->ops && item->ops->send);
	return item->ops->send(item, fd, fdtype);
}

INLINE size_t output_remaining(output_item *item) {
	assert(item->ops && item->ops->remaining);
	return item->ops->remaining(item);
}

INLINE void output_on_complete(output_item *item) {
	assert(item->ops);
	if(item->on_complete)
		item->on_complete(item);
	if(item->ops->on_complete)
		item->ops->on_complete(item);
}

INLINE void output_on_error(output_item *item, int status) {
	assert(item->ops);
	if(item->on_error)
		item->on_error(item, status);
	if(item->ops->on_error)
		item->ops->on_error(item, status);
}
INLINE void output_release(output_item *item) {
	assert(item->ops && item->ops->release);
	item->ops->release(item);
}
