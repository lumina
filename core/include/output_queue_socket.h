#ifndef OUTPUT_QUEUE_SOCKET_H
#define OUTPUT_QUEUE_SOCKET_H

#include "config.h"
#include "data_item.h"
#include "core.h"


Target *new_Target_Socket(OutputQueue *queue, int fd);
void free_Target_Socket(OutputQueue *queue, Target *target);
OutputQueue *new_OutputQueue_Socket(core_t *core);

#endif
