#ifndef OUTPUT_HOOK_H
#define OUTPUT_HOOK_H

#include <ev.h>
#include "output_item.h"

struct output_hook;
typedef struct output_hook output_hook;

output_hook *new_output_hook();
void release_output_hook(output_hook *hook);

void output_hook_queue(output_hook *hook, output_item *item);

void output_hook_callback(EV_P_ ev_io *w, int revents);

#endif
