#ifndef OUTPUT_MEMORY_H
#define OUTPUT_MEMORY_H

#include "output_item.h"

/* Memory output item
 * Either creates a copy of the data, or is given ownership
 * Cannot be added multiple times due to lack of reference counting/etc since object self-frees
 */

typedef struct {
	output_item item;
	BYTE *data;
	size_t size;
	size_t index;
} memory_output_item;

output_item *new_memory_output_item(void *data, size_t size, int copy);

#endif
