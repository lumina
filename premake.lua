project.name = "lumina"
project.libdir = "lib"
project.bindir = "bin"

dopackage("core")
dopackage("test")
dopackage("tests")
