#ifndef OUTPUT_FILE_H
#define OUTPUT_FILE_H

#include "output_item.h"

/* File output item
 * Either creates a copy of the data, or is given ownership
 * Cannot be added multiple times due to lack of reference counting/etc since object self-frees
 */

typedef struct {
	output_item item;
	int fd;
	off_t size;
	off_t index;
	int no_sendfile;
} file_output_item;

output_item *new_file_output_item(int fd);

#endif
