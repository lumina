Core:

	dns_handler
	connect_handler
	listen_handler
	output_handler
	input_handler

-all requests-
	request_set_callback(req, complete, error)

-all handlers-
	queue_request (h, req)

dns_handler:

	new_dns_request (h, node, service, hints)
	dns_request_get_sockaddr (dr, sockaddr, len)

dns_request:

	+on_error (dr, ior_status) : ior_status
	+on_complete (dr)

connect_handler:

	new_connect_request (h, sockaddr, len)
	connect_request_set_socket (cr, fd)
	connect_request_set_create_params (cr, domain, type, protocol)
	connect_request_get_fd (cr) : fd

connect_request:

	+on_error (cr, ior_status) :ior_status
	+on_complete (cr)

listen_handler:

	new_listen_request (h, sockaddr, len, persistent)
	listen_request_set_socket (cr, fd)
	listen_request_set_create_params (cr, domain, type, protocol)
	listen_request_pop_fd (lr) : fd

listen_request:

	+on_error (lr, ior_status) :ior_status
	+on_complete (lr)

-- if an *_item wants to be re-used, it must clone

output_handler:

	new_*_item (h, fd, fdtype, ...)

input_handler:

	new_*_item (h, fd, fdtype, ...)

ior_status:

	MORE - more data left to send, the hook should wait
	HOLD - unhook write events (something else will rehook them up)
	DONE - unqueue me
	ABORT - should 'abort' all requests (kind-of-like ERROR_CLOSED except not an error)
	ERROR_CLOSED - other end closed
	ERROR_ERRNO - unknown error, contained in errno
	ERROR_INPUT - some error obtaining the data to write
	ERROR_OUTPUT - some error putting the read data to output

[+ callback both in request-ops and in individual request to allow user handling as well as req]

output_request:

	prepare (or)
	send (or, fd, fdtype) : ior_status
	release (or)
	+on_complete (or)
	+on_error (or, ior_status) : ior_status - called inside of send or prepare, (can return MORE, HOLD, DONE, ABORT)

input_request:

	prepare (ir)
	recv (ir, fd, fdtype) : ior_status
	release (ir)
	+on_complete (ir)
	+on_error (ir, ior_status) : ior_status - called inside of recv or prepare, (can return MORE, HOLD, DONE, ABORT)
