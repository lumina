#include "data_item/memory_data_item.h"

#include <memory.h> /* memcpy */
#include <stdio.h> /* fprintf */
#include <stdlib.h> /* malloc, free, abort */

static void free_MemoryItem(DataItem *item);
static void MemoryItem_clear(DataItem *item);

static const char *memoryTypeName[] = { "Memory", NULL };

static DataItem_ops memoryOps = {
	free_MemoryItem,
	MemoryItem_clear,
	DATA_ITEM_MEMORY,
	memoryTypeName
};
/* New memoryitem does not take ownership of the buffer, but clones it */
DataItem *new_MemoryItem(MemoryBuffer buffer) {
	MemoryItem *item = calloc(1, sizeof(MemoryItem));
	if(!item) goto fail;
	item->base.ops = &memoryOps;
	item->buffer = buffer;
	item->buffer.data = malloc(buffer.len);
	if(!item->buffer.data) goto fail;
	memcpy(item->buffer.data, buffer.data, buffer.len);
	return (DataItem*)item;
fail:
	if(item && item->buffer.data) free(item->buffer.data);
	if(item) free(item);
	return NULL;
}

static void MemoryItem_clear(DataItem *item) {
	MemoryItem *mi = (MemoryItem*)item;
	if(mi->buffer.data) {
		free(mi->buffer.data);
		mi->buffer.data = NULL;
	}
}
static void free_MemoryItem(DataItem *item) {
	MemoryItem_clear(item);
	free(item);
}
