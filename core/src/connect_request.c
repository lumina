#include "connect_request.h"
#include "request.h"
#include "core.h"

#include <sys/fcntl.h> /* fcntl */

#include <memory.h>
#include <stdlib.h> /* calloc */
#include <errno.h>  /* errno */
#include <ev.h>

#include "socket_request.h"

struct ConnectRequest {
	core_t *core;
	HandlerCallback callback;
	ev_io connectWatcher;
	ev_timer killTimer;
	int fd;
	/* XXX: Probably could be removed if queue operation accepted sockaddr ... */
	struct sockaddr *addr;
	socklen_t addr_len;
};

ConnectRequest *new_ConnectRequest(core_t *c, HandlerCallback callback, int fd, const struct sockaddr *addr, socklen_t addr_len) {
	ConnectRequest *cr = calloc(1, sizeof(ConnectRequest));
	if(!cr) goto fail;
	cr->core = c;
	cr->callback = callback;
	cr->fd = fd;
	cr->addr = malloc(addr_len);
	if(!cr->addr) goto fail;
	memcpy(cr->addr, addr, addr_len);
	cr->addr_len = addr_len;
	return cr;
fail:
	free_ConnectRequest(cr);
	return NULL;
}

void free_ConnectRequest(ConnectRequest *cr) {
	if(!cr) return;
	if(cr->addr) free(cr->addr);
	if(ev_is_active(&cr->connectWatcher))
		ev_io_stop(cr->core->loop, &cr->connectWatcher);
	free(cr);
}

int ConnectRequest_getFd(ConnectRequest *cr) {
	return cr->fd;
}

core_t *ConnectRequest_getCore(ConnectRequest *cr) {
	return cr->core;
}

static void connect_cb(struct ev_loop *loop, struct ev_io *w, int revents) {
	ConnectRequest *cr = w->data;
	int fd = cr->fd;
	int err;
	socklen_t errlen = sizeof(err);
	/* Be it error or success.. the IO watcher needs to stop */
	ev_io_stop(loop, w);
	ev_timer_stop(loop, &cr->killTimer);
	if(!(revents & EV_WRITE)) { /* TODO: error ? */
		return;
	}
	/* Check for connection error */
	getsockopt(fd, SOL_SOCKET, SO_ERROR, &err, &errlen);
	if(err == 0) {
		cr->callback.onComplete(cr, cr->callback.data);
		return;
	}
	errno = err;
	cr->callback.onError(cr, IOR_ERROR_ERRNO, cr->callback.data);
}

static void kill_cb(struct ev_loop *loop, struct ev_timer *w, int revents) {
	ConnectRequest *cr = w->data;
	ev_io_stop(loop, &cr->connectWatcher);
	ev_timer_stop(loop, w);
	errno = ETIMEDOUT;
	cr->callback.onError(cr, IOR_ERROR_ERRNO, cr->callback.data);
}

static void setup_connect(ConnectRequest *cr, double timeout) {
	int fd = cr->fd;
	int flags;
	/* Make sure the socket is non-blocking */
	/* XXX: Probably should remove since all sockets coming
	 * into our stuff SHOULD be N/B */
	if(-1 == (flags = fcntl(fd, F_GETFL, 0)))
		flags = 0;
	fcntl(fd, F_SETFL, flags | O_NONBLOCK);

tryagain:
	if(0 != connect(cr->fd, cr->addr, cr->addr_len)) {
		if(errno == EINTR) goto tryagain;
		if(errno == EINPROGRESS) {
			/* setup write watcher */
			ev_io_init(&cr->connectWatcher, connect_cb, cr->fd, EV_WRITE);
			cr->connectWatcher.data = cr;
			ev_io_start(cr->core->loop, &cr->connectWatcher);
			/* setup kill timer */
			ev_timer_init(&cr->killTimer, kill_cb, timeout, 0);
			cr->killTimer.data = cr;
			ev_timer_start(cr->core->loop, &cr->killTimer);
		} else {
			cr->callback.onError(cr, IOR_ERROR_ERRNO, cr->callback.data);
		}
	} else {
		cr->callback.onComplete(cr, cr->callback.data);
	}
}

void ConnectRequest_queue(ConnectRequest *cr, double timeout) {
	setup_connect(cr, timeout);
}

