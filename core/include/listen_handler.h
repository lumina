#ifndef LISTEN_HANDLER_H
#define LISTEN_HANDLER_H

#include "config.h"

#include <sys/types.h>
#include <sys/socket.h> /* sockaddr */

struct listen_handler {
	core_t *core;
};

listen_handler_t *new_listen_handler(core_t *c);
void free_listen_handler(listen_handler_t *h);

listen_request_t *new_listen_request(listen_handler_t *h, const struct sockaddr *addr, socklen_t addr_len, int persistent);

void listen_request_set_socket(listen_request_t *cr, int fd);

void listen_request_set_create_params(listen_request_t *cr, int domain, int type, int protocol);

void listen_handler_queue(listen_handler_t *h, listen_request_t *lr);

#endif
