#include "socket_request.h"

#include "core.h"

#include <ev.h>

#include <stdlib.h> /* calloc */
#include <errno.h>  /* errno */
#include <sys/fcntl.h>
#include <sys/socket.h> /* socket */

/* If the values passed in are zero then use these */
#define DEFAULT_RETRY_ITERATIONS 10
#define DEFAULT_RETRY_INTERVAL (0.25)

/* ISO: unnamed unions not allows... */

struct SocketRequest {
	core_t *core;
	ev_timer retryTimer;
	union {
		int triesLeft;
		int fd;
	};
	HandlerCallback callback;
	byte domain, type, protocol;
};

/* Initialize a new SocketRequest instance
 * Technically 'socket' accepts integer values, but I have not seen values outside the range
 * so I "optimize" and limit it to the byte range.
 * RFC: Should I expand the range?  If so, how much?
 */
SocketRequest *new_SocketRequest(core_t *core, HandlerCallback callback, byte domain, byte type, byte protocol) {
	SocketRequest *ret = calloc(1, sizeof(SocketRequest));
	if(ret == NULL) return NULL;
	ret->core = core;
	ret->domain = domain;
	ret->type = type;
	ret->protocol = protocol;
	ret->callback = callback;
	return ret;
}

/* Release the SocketRequest.
 * If it was waiting, stop the timer
 */
void free_SocketRequest(SocketRequest *sc) {
	if(!sc) return;
	if(ev_is_active(&sc->retryTimer)) {
		ev_timer_stop(sc->core->loop, &sc->retryTimer);
	}
	free(sc);
}

int SocketRequest_getFd(SocketRequest *sc) {
	return sc->fd;
}

core_t *SocketRequest_getCore(SocketRequest *sc) {
	return sc->core;
}

static void setupSocket_cb(struct ev_loop *loop, struct ev_timer *w, int revents);

static void setupSocket(SocketRequest *sc) {
	int flags;
	int fd = socket(sc->domain, sc->type, sc->protocol);
	if(fd == -1) {
		/* For this 'retry' approach to no files remaining, we
		 * cannot ignore the event when its the last permitted try */
		if(sc->triesLeft >= 0 && (errno == ENFILE || errno == EMFILE)) { /* no files remaining - should retry */
			if(ev_is_active(&sc->retryTimer)) return;
			ev_timer_start(sc->core->loop, &sc->retryTimer);
		} else { /* another error occurred... */
			if(ev_is_active(&sc->retryTimer))
				ev_timer_stop(sc->core->loop, &sc->retryTimer);
			sc->fd = -1; /* Reset FD to <= 0 in case error handler thinks it wants to close */
			sc->callback.onError(sc, IOR_ERROR_ERRNO, sc->callback.data);
		}
		return;
	}
	/* begin connection process.. */
	if(ev_is_active(&sc->retryTimer))
		ev_timer_stop(sc->core->loop, &sc->retryTimer);

	/* Socket's ready, now initialize */
	/* Make sure the socket is non-blocking */
	if(-1 == (flags = fcntl(fd, F_GETFL, 0)))
		flags = 0;
	fcntl(fd, F_SETFL, flags | O_NONBLOCK);

	sc->fd = fd;
	sc->callback.onComplete(sc, sc->callback.data);
}

static void setupSocket_cb(struct ev_loop *loop, struct ev_timer *w, int revents) {
	SocketRequest *sc = w->data;
	sc->triesLeft--;
	setupSocket(sc);
}


/* Queue up the SocketRequest figuratively
 * Attempts to allocate a socket with the given parameter...
 * If it fails, check the error and optionally start retrying
 * Else set the non-blocking flag on the socket and pass it off
 */
void SocketRequest_queue(SocketRequest *sc, double retryInterval, int triesLeft) {
	/* Initialize the timer w/ the retry interval
	 * Note that this merely sets values, no allocation occurs */
	if(retryInterval == 0.) retryInterval = DEFAULT_RETRY_INTERVAL;
	if(triesLeft == 0) triesLeft = DEFAULT_RETRY_ITERATIONS;
	ev_timer_init(&sc->retryTimer, setupSocket_cb, 0., retryInterval);
	sc->retryTimer.data = sc;
	sc->triesLeft = triesLeft;

	/* Attempt to create the socket immediately and pass it on */
	setupSocket(sc);
}
