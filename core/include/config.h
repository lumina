#ifndef CONFIG_H
#define CONFIG_H

#include "stdlib.h"
#include "types.h"

#ifndef INLINE
#ifndef __GNUC__
#define INLINE static __inline__
#else
#define INLINE static inline
#endif
#endif

enum ior_status {
	IOR_MORE,
	IOR_HOLD,
	IOR_DONE,
	IOR_ABORT,
	IOR_ERROR_CLOSED,
	IOR_ERROR_ERRNO,
	IOR_ERROR_INPUT,
	IOR_ERROR_OUTPUT
};

#endif
