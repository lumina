#ifndef DNS_HANDLER_H

#include "config.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h> /* addrinfo */

struct dns_handler {
	core_t *core;
};

dns_handler_t *new_dns_handler(core_t *c);
void free_dns_handler(dns_handler_t *h);

dns_request_t *new_dns_request(dns_handler_t *h, const char *node, const char *service, const struct addrinfo *hints);

void dns_handler_queue(dns_handler_t *h, dns_request_t *dr);

#endif
