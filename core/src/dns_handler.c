#include "dns_handler.h"
#include <stdlib.h> /* calloc */

dns_handler_t *new_dns_handler(core_t *c) {
	dns_handler_t *h = calloc(1, sizeof(dns_handler_t));
	if(!h) goto fail;
	h->core = c;
	return h;
fail:
	free_dns_handler(h);
	return NULL;
}

void free_dns_handler(dns_handler_t *h) {
	if(!h) return;
	free(h);
}
