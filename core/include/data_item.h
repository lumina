#ifndef DATA_ITEM_H
#define DATA_ITEM_H

#include "config.h"
#include <sys/queue.h>

#include <assert.h>

typedef enum DataItemType {
	DATA_ITEM_NULL = 0,
	DATA_ITEM_MEMORY
} DataItemType;

typedef struct DataItem DataItem;
typedef struct DataItem_ops DataItem_ops;

STAILQ_HEAD(DataItem_queue, DataItem);
typedef struct DataItem_queue DataItem_queue;

struct DataItem_ops {
	/** Completely 'free' all resources of the DataItem */
	void (*free)(DataItem *);
	/** Release expensive resources, leaving identity information */
	void (*clear)(DataItem *);
	enum DataItemType typeID;
	const char **fullType;
};

struct DataItem {
	DataItem_ops *ops;
	STAILQ_ENTRY(DataItem) entries;
};

INLINE void free_DataItem(DataItem *item) {
	assert(item && item->ops && item->ops->free);
	item->ops->free(item);
}

INLINE void DataItem_clear(DataItem *item) {
	assert(item && item->ops && item->ops->clear);
	item->ops->clear(item);
}

INLINE DataItemType DataItem_getTypeID(DataItem *item) {
	assert(item && item->ops);
	return item->ops->typeID;
}

INLINE const char** DataItem_getFullType(DataItem *item) {
	assert(item && item->ops);
	return item->ops->fullType;
}

#endif
