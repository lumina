#include "core.h"

#include "dns_handler.h"
#include "listen_handler.h"
#include "input_handler.h"

#include <stdlib.h> /* calloc */
#include <ev.h>


#define CHECK_ALLOC(item) if(!(c->item = new_ ## item ## _handler(c))) goto fail;

core_t *new_core() {
	core_t *c = calloc(1, sizeof(core_t));
	if(c == NULL) goto fail;
	c->loop = ev_loop_new(0);
	if(!c->loop) goto fail;
	CHECK_ALLOC(dns);
	CHECK_ALLOC(listen);
	CHECK_ALLOC(input);
	return c;
fail:
	free_core(c);
	return NULL;
}

void free_core(core_t *c) {
	if(!c) return;
	if(c->dns)     free_dns_handler(c->dns);
	if(c->listen)  free_listen_handler(c->listen);
	if(c->input)   free_input_handler(c->input);
	if(c->loop)    ev_loop_destroy(c->loop);
	free(c);
}
