#include "output/file.h"

#include <errno.h>
#include <sys/types.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <unistd.h> /* write */
#include <stdlib.h> /* malloc, free */
#include <string.h> /* memcpy */

static size_t file_get_size(output_item *this);
static int file_send(output_item *this, int fd, int fdtype);
static size_t file_remaining(output_item *this);
static void file_release(output_item *this);

static size_t file_get_size(output_item *this) {
	return ((file_output_item*)this)->size;
}

static int file_send(output_item *this, int fd, int fdtype) {
	file_output_item *mthis = (file_output_item*)this;
	/* Zero remaining.. this shouldn't occur in workflow */
	size_t to_send = file_remaining(this);
	if(to_send == 0) return 0;
	/* Expand FDT_SOCKET to use flags (ex: for MSG_MORE) in certain situations (new output_type?) */
	switch(fdtype) {
	case FDT_SOCKET: /* might be able to use sendfile here */
		if(!mthis->no_sendfile) {
			ssize_t sent = sendfile(fd, mthis->fd, &mthis->index, to_send);
			if(sent >= 0)
				return sent;
			if(!(errno == EINVAL || errno == ENOSYS))
				return sent;
			mthis->no_sendfile = 1;
		}
	case FDT_FILE:
	case FDT_PIPE: {
		/* TODO: Need to figure out how to read from buffer async and pipe that to out */
		char buffer[1024];
		ssize_t dataRead = read(mthis->fd, buffer, sizeof(buffer));
		int index = 0;
		mthis->index += dataRead;
		while(dataRead > 0) {
			/* NOTE: Due to nature of data input... output will necessarily block
			 * or go into loop until a non-block op works */
			ssize_t written = write(fd, buffer + index, dataRead);
			if(written > 0) {
				dataRead -= written;
				index += written;
			} else {
				/* Bailed/closed, oh no! */
				if(errno != EAGAIN || written == 0)
					return written;
			}
		}
		return index;
	}
	default:
		/* XXX: Raise error */
		return -1;
	}
}

static size_t file_remaining(output_item *this) {
	file_output_item *mthis = (file_output_item*)this;
	return mthis->size - mthis->index;
}

static void file_release(output_item *this) {
	file_output_item *mthis = (file_output_item*)this;
	if(mthis->fd)
		close(mthis->fd);
	free(this);
}

static output_item_ops file_output_item_ops = {
	.get_size = file_get_size,
	.send = file_send,
	.remaining = file_remaining,
	.release = file_release
};

/* If an error occurs, fd doesn't changes ownership */
output_item *new_file_output_item(int fd) {
	file_output_item *mitem;
	struct stat buf;
	if(0 != fstat(fd, &buf))
		return NULL;
	/* FD Must point to a real file */
	if(!S_ISREG(buf.st_mode))
		return NULL;
	mitem = calloc(1, sizeof(file_output_item));
	if(!mitem) return NULL;
	mitem->item.ops = &file_output_item_ops;
	mitem->fd = fd;
	mitem->size = buf.st_size;
	mitem->index = 0;
	return (output_item*)mitem;
}
