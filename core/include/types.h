#ifndef TYPES_H
#define TYPES_H

struct core;
struct dns_handler;
struct listen_handler;
struct input_handler;

typedef struct core core_t;
typedef struct dns_handler dns_handler_t;
typedef struct listen_handler listen_handler_t;
typedef struct input_handler input_handler_t;

struct request;
typedef struct request request_t;

struct dns_request;
typedef struct dns_request dns_request_t;

struct listen_request;
typedef struct listen_request listen_request_t;

struct input_request;
typedef struct input_request input_request_t;


struct SocketRequest;
typedef struct SocketRequest SocketRequest;

struct ConnectRequest;
typedef struct ConnectRequest ConnectRequest;

typedef unsigned char byte;

#endif
