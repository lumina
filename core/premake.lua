package.name = "lumina.core"
package.kind = "dll"
package.language = "c++"

package.links = {
	"ev"
}

package.includepaths = {
	"include",
}
if linux then
	package.buildoptions = { "-Wall" }
	package.config["Debug"].buildoptions = { "-O0" }
else
	print([[Other environments currently untested, may need tweaking]])
end

package.files = {
	matchrecursive(
		"src/*.c",
		"include/*.h"
	)
}
