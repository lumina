#include "output/memory.h"

#include <unistd.h> /* write */
#include <stdlib.h> /* malloc, free */
#include <string.h> /* memcpy */

static size_t memory_get_size(output_item *this);
static int memory_send(output_item *this, int fd, int fdtype);
static size_t memory_remaining(output_item *this);
static void memory_release(output_item *this);

static size_t memory_get_size(output_item *this) {
	return ((memory_output_item*)this)->size;
}

static int memory_send(output_item *this, int fd, int fdtype) {
	int ret;
	memory_output_item *mthis = (memory_output_item*)this;
	/* Zero remaining.. this shouldn't occur in workflow */
	size_t to_send = memory_remaining(this);
	if(to_send == 0) return 0;
	/* Expand FDT_SOCKET to use flags (ex: for MSG_MORE) in certain situations (new output_type?) */
	switch(fdtype) {
	case FDT_SOCKET:
	case FDT_FILE:
	case FDT_PIPE:
		ret = write(fd, mthis->data + mthis->index, to_send);
		/* Move the ptr forward */
		if(ret > 0)
			mthis->index += ret;
		/* XXX: Handle error */
		return ret;
	default:
		/* XXX: Raise error */
		return -1;
	}
}

static size_t memory_remaining(output_item *this) {
	memory_output_item *mthis = (memory_output_item*)this;
	return mthis->size - mthis->index;
}

static void memory_release(output_item *this) {
	memory_output_item *mthis = (memory_output_item*)this;
	if(mthis->data)
		free(mthis->data);
	mthis->data = NULL;
	free(this);
}

static output_item_ops memory_output_item_ops = {
	.get_size = memory_get_size,
	.send = memory_send,
	.remaining = memory_remaining,
	.release = memory_release
};

/* If an error occurs, and 'owns' is set, the data never changes ownership */
output_item *new_memory_output_item(void *data, size_t size, int owns) {
	memory_output_item *mitem = calloc(1, sizeof(memory_output_item));
	if(!mitem) return NULL;
	mitem->item.ops = &memory_output_item_ops;
	if(!owns) {
		void *newdata = malloc(size);
		if(!newdata) return NULL;
		memcpy(newdata, data, size);
		data = newdata;
	}
	mitem->data = data;
	mitem->size = size;
	mitem->index = 0;
	return (output_item*)mitem;
}
