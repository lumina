#ifndef FILTER_CHAIN_H
#define FILTER_CHAIN_H

#include "config.h"
#include "filter.h"

typedef void (*Filter_stateRelease)(void *);

void FilterChain_action(void *state, FilterMode mode, DataItem *item, FilterAction_next next, void *nextData);

void *new_FilterChain();
void free_FilterChain(void *state);
void FilterChain_append(void *state, FilterAction action, void *actionState, Filter_stateRelease release);
void FilterChain_prepend(void *state, FilterAction action, void *actionState, Filter_stateRelease release);

/* NOTE: Removal actions and deletion of filter state may need some workflow design
 * since if any item is removed while its being used, problems will ensue */
#endif
