#include <check.h>
#include <signal.h>

#include "test_list.h"

#include "filter.h"

/* Mock actions for dispatch tests, the data item is altered if passed through, nextData will be the old value of the item */
static void action_bad(void* filter, FilterMode mode, DataItem *item, FilterAction_next next, void* nextData) {
	fail("Incorrect action was called");
}
static void action_good(void* filter, FilterMode mode, DataItem *item, FilterAction_next next, void* nextData) {
	if(next)
		next(item, mode, item);
}
static void action_unaltered_next(void* nextData, FilterMode mode, DataItem *item) {
	fail_unless(nextData != item, "A proper callback was not made");
}
static void action_altered_next(void* nextData, FilterMode mode, DataItem *item) {
	fail_unless(nextData == item, "A proper callback was not made");
}
START_TEST(test_SimpleFilter_action_dispatch) {
	SimpleFilter f = {
		action_bad,
		action_bad,
		action_bad,
		action_bad
	};
	SimpleFilter *pf = &f;
	mark_point();
	/* Test for invalid dispatch for unknown mode */
	SimpleFilter_action(pf, -1, NULL, action_unaltered_next, pf);
	/* Test passthrough of NULL functions */
	f.onWrite = NULL;
	SimpleFilter_action(pf, FILTER_WRITE, NULL, action_unaltered_next, pf);
	f.onWrite = action_good;
	SimpleFilter_action(pf, FILTER_WRITE, NULL, action_altered_next, pf);
	f.onWrite = action_bad;
	f.onWriteComplete = action_good;
	mark_point();
	SimpleFilter_action(pf, FILTER_WRITE_COMPLETE, NULL, action_altered_next, pf);
	f.onWriteComplete = action_bad;
	f.onRead = action_good;
	mark_point();
	SimpleFilter_action(pf, FILTER_READ, NULL, action_altered_next, pf);
	f.onRead = action_bad;
	f.onReadComplete = action_good;
	mark_point();
	SimpleFilter_action(pf, FILTER_READ_COMPLETE, NULL, action_altered_next, pf);
}
END_TEST

/* callback that ensures that 'next' is called by assume that the nextData ptr is a *int and sets it to the mode value */
static void action_mark_called(void* nextData, FilterMode mode, DataItem *item) {
	int *data = (int*)nextData;
	*data = mode;
}

START_TEST(test_SimpleFilter_action_next) {
	SimpleFilter f = {
		NULL,
		action_bad,
		action_bad,
		action_bad
	};
	SimpleFilter *pf = &f;
	int data = 0;
	SimpleFilter_action(pf, FILTER_WRITE, NULL, action_mark_called, &data);
	fail_unless(data == FILTER_WRITE, "The 'next' action was not called");
}
END_TEST

/* callback that does nothing */
static void action_null_next(void *nextData, FilterMode mode, DataItem *item) {
}

START_TEST(test_SimpleFilter_require_filter) {
	SimpleFilter_action(NULL, FILTER_WRITE, NULL, action_null_next, NULL);
}
END_TEST
START_TEST(test_SimpleFilter_require_next) {
	SimpleFilter f = {
		action_bad,
		action_bad,
		action_bad,
		action_bad
	};
	SimpleFilter_action(&f, FILTER_WRITE, NULL, NULL, NULL);
}
END_TEST
Suite* new_filter_suite() {
	Suite *s = suite_create("filter");
	
	/* Core test case */
	TCase *tc_SimpleFilter = tcase_create("SimpleFilter");
	tcase_add_test(tc_SimpleFilter, test_SimpleFilter_action_dispatch);
	tcase_add_test(tc_SimpleFilter, test_SimpleFilter_action_next);
	tcase_add_test_raise_signal(tc_SimpleFilter, test_SimpleFilter_require_filter, SIGABRT);
	tcase_add_test_raise_signal(tc_SimpleFilter, test_SimpleFilter_require_next, SIGABRT);
	suite_add_tcase(s, tc_SimpleFilter);
	return s;
}
