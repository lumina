#include "output_handler.h"
#include <malloc.h> /* calloc */

output_handler_t *new_output_handler(core_t *c) {
	output_handler_t *h = calloc(1, sizeof(output_handler_t));
	if(!h) goto fail;
	h->core = c;
	return h;
fail:
	free_output_handler(h);
	return NULL;
}

void free_output_handler(output_handler_t *h) {
	if(!h) return;
	free(h);
}
